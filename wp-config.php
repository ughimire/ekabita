<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ekabita_NEW');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'SpR-;<=cVRxLqD|mf.SJS&K-#<fNkUV[0Zjc/6?a0bV*;0=NBFie`x%yn:Y.Kucc');
define('SECURE_AUTH_KEY',  ':D5v;O:=sis,?HKvc{K>8cDMGV;Ff*H|/,_!$-;V,Y/IN-mT)C,.`aCuVfNxaTLu');
define('LOGGED_IN_KEY',    '1Dfck_m~p2q(/]&.rAx ^nLOxuD2ZSvJpgF/I%pkAzM0[DX)p_lo?e+ #;bD>e<[');
define('NONCE_KEY',        'o7jJ|-N@Yl] I#0^I(idlnW{6QdO}m6LW|+ X!kyb6o-LS8,N|;ST)AWs.k+PRQ/');
define('AUTH_SALT',        'TS;&VTj2 Ik|6|9VXZL1C%eZy42J.jcCZ+C{i0/0Y,;%Xz../Pg}XSF@0~~^R*@u');
define('SECURE_AUTH_SALT', 'gRw.pdj<fQjs4o0Ouk71|JEj7p9zO2j/`GZX+.ZDG}:3(kA|`^8|P/_?4%[$7:5b');
define('LOGGED_IN_SALT',   '.bwQ1g[;qeuPvTLR6`04mz*DM-e@k&FuFV,8P9+|q_jpiso-)+=JrxJ2|fa7{lw:');
define('NONCE_SALT',       't1i}-O~gY_lT4Efafl,Cs{f=bLK(}7C#[Tv]S{*#F~R-+c>#xjE~R u1O)[(fEek');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'kabita_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
